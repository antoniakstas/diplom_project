terraform{
	required_providers{
                libvirt = {
                        source = "dmacvicar/libvirt"
                        version = "0.6.2"
                }
        }
}

provider "libvirt" {
        uri = "qemu:///system"
}

resource "libvirt_volume" "diplom_kvm-qcow2" {
  name = "diplom_kvm.qcow2"
  pool = "default"
  source = "/home/imgs/test.qcow2"
  format = "qcow2"
}

resource "libvirt_domain" "diplom_kvm" {
  name   = "diplom_kvm"
  memory = "2048"
  vcpu   = 2

  network_interface {
    network_name = "br1"
  }

  disk {
    volume_id = "${libvirt_volume.diplom_kvm-qcow2.id}"
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}
output "ip" {
  value = "${flatten("${libvirt_domain.diplom_kvm.*.network_interface.0.addresses}")}"
}
