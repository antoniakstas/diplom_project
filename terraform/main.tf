terraform{
	required_providers{
                libvirt = {
                        source = "dmacvicar/libvirt"
                        version = "0.6.2"
                }
        }
}

provider "libvirt" {
        uri = "qemu:///system"
}

resource "libvirt_volume" "test1-qcow2" {
  name = "test1.qcow2"
  pool = "default"
  source = "/home/imgs/test.qcow2"
  format = "qcow2"
}

resource "libvirt_domain" "test1" {
  name   = "test1"
  memory = "1024"
  vcpu   = 1

  network_interface {
    network_name = "br1"
  }

  disk {
    volume_id = "${libvirt_volume.test1-qcow2.id}"
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}
output "ip" {
  value = "${flatten("${libvirt_domain.test1.*.network_interface.0.addresses}")}"
}
